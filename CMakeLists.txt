cmake_minimum_required(VERSION 3.5)
project(alpha_emu_gazebo_plugin)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wpedantic)
endif()


# find dependencies
find_package(ament_cmake REQUIRED)
find_package(gazebo_dev REQUIRED)
find_package(gazebo_ros REQUIRED)
find_package(geometry_msgs REQUIRED)  
find_package(nav_msgs REQUIRED)
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)
find_package(tf2 REQUIRED)
find_package(tf2_geometry_msgs REQUIRED)
find_package(tf2_ros REQUIRED)
find_package(alpha_emu_msgs REQUIRED)                   


include_directories(${GAZEBO_INCLUDE_DIRS})
link_directories(${GAZEBO_LIBRARY_DIRS})


add_library(ackermann_drive_ros_gazebo_plugin SHARED ./src/ackermann_drive_ros_gazebo_plugin.cpp)
target_include_directories(ackermann_drive_ros_gazebo_plugin PUBLIC ./include)
target_include_directories(ackermann_drive_ros_gazebo_plugin PRIVATE ./src)


target_link_libraries(ackermann_drive_ros_gazebo_plugin
  ${GAZEBO_LIBRARIES}
  ${rclcpp_LIBRARIES}
  ${std_msgs_LIBRARIES}
  ${nav_msgs_LIBRARIES}
  ${tf2_geometry_msgs_LIBRARIES}
)

ament_target_dependencies(ackermann_drive_ros_gazebo_plugin 
  "gazebo_dev"
  "gazebo_ros"
  "rclcpp"
  "std_msgs"
  "tf2"
  "tf2_geometry_msgs"
  "tf2_ros"
  "nav_msgs"
  "alpha_emu_msgs"
) 



install(TARGETS ackermann_drive_ros_gazebo_plugin
  DESTINATION lib
)

ament_environment_hooks("${CMAKE_CURRENT_SOURCE_DIR}/env-hooks/alpha_emu_gazebo_plugin.sh.in")

ament_export_libraries(ackermann_drive_ros_gazebo_plugin)

ament_export_include_directories(include)
ament_export_dependencies(rclcpp)
ament_export_dependencies(gazebo_dev)
ament_export_dependencies(gazebo_ros)

ament_package()
